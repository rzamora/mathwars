#pragma once
#include "System.h"

class Menu : public System {
public:
	Menu();
	void Update();
	void ProcessMessage(const char* msg);
};

