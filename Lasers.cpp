#include "Lasers.h"
#include "api.h"

#define MAX_LASERS 5
struct Laser {
	Point ini, fin;
	float pos;
	unsigned char r, g, b;
};
Laser lasers[MAX_LASERS];
int randomLaserCounter = 60;

Lasers::Lasers() {
	randomLaserCounter = 60;
	for (int i = 0; i < MAX_LASERS; i++) lasers[i].pos = -1.0f;
	RegisterMessage("CreateNewLaser", this);
}

void GenerateNewLaser() {
	randomLaserCounter = rand() % 60;
	for (int i = 0; i < MAX_LASERS; i++) {
		if (lasers[i].pos < 0.0f) {
			if (rand() % 2 == 0) {
				lasers[i].ini.x = 0;
				lasers[i].ini.y = rand() % 270;
				lasers[i].fin.x = 480;
				lasers[i].fin.y = (rand() % 200) - 100;
				lasers[i].pos = 0.0f;
				lasers[i].r = 255; lasers[i].g = 0; lasers[i].b = 0;
			}
			else {
				lasers[i].ini.x = 480;
				lasers[i].ini.y = rand() % 270;
				lasers[i].fin.x = -480;
				lasers[i].fin.y = (rand() % 200) - 100;
				lasers[i].pos = 0.0f;
				lasers[i].r = 0; lasers[i].g = 255; lasers[i].b = 0;
			}
			return;
		}
	}
}

void Lasers::Update() {
	randomLaserCounter--;
	if (randomLaserCounter <= 0) GenerateNewLaser();
	for (int i = 0; i < MAX_LASERS; i++) {
		if (lasers[i].pos >= 0.0f) {
			lasers[i].pos += 0.1f;
			SetColor(lasers[i].r, lasers[i].g, lasers[i].b);
			DrawLine(lasers[i].ini.x + lasers[i].fin.x*lasers[i].pos, lasers[i].ini.y + lasers[i].fin.y*lasers[i].pos, lasers[i].ini.x + lasers[i].fin.x*(lasers[i].pos + 0.2f), lasers[i].ini.y + lasers[i].fin.y*(lasers[i].pos + 0.2f));
			DrawLine(lasers[i].ini.x + lasers[i].fin.x*lasers[i].pos, 0.5 + lasers[i].ini.y + lasers[i].fin.y*lasers[i].pos, lasers[i].ini.x + lasers[i].fin.x*(lasers[i].pos + 0.2f), 0.5 + lasers[i].ini.y + lasers[i].fin.y*(lasers[i].pos + 0.2f));
			DrawLine(lasers[i].ini.x + lasers[i].fin.x*lasers[i].pos, 1 + lasers[i].ini.y + lasers[i].fin.y*lasers[i].pos, lasers[i].ini.x + lasers[i].fin.x*(lasers[i].pos + 0.2f), 1 + lasers[i].ini.y + lasers[i].fin.y*(lasers[i].pos + 0.2f));
			if (lasers[i].pos >= 0.8f)
				lasers[i].pos = -1.0f;
		}
	}
}

void CreateNewLaser(float x1, float y1, float x2, float y2, unsigned char r, unsigned char g, unsigned char b) {
	for (int i = 0; i < MAX_LASERS; i++) {
		if (lasers[i].pos < 0.0f) {
			lasers[i].ini.x = x1;
			lasers[i].ini.y = y1;
			lasers[i].fin.x = x2 - x1;
			lasers[i].fin.y = y2 - y1;
			lasers[i].pos = 0.0f;
			lasers[i].r = r; lasers[i].g = g; lasers[i].b = b;
			return;
		}
	}
}

void Lasers::ProcessMessage(const char* msg) {
	if (message_eq("CreateNewLaser")) {
		int* params = GetMessageParams();
		if (params[4] == 0) {
			CreateNewLaser(params[0], params[1], params[2], params[3], 255, 0, 0);
		} else {
			CreateNewLaser(params[0], params[1], params[2], params[3], 0, 255, 0);
		}
	}
}