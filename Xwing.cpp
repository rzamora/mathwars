#include "Xwing.h"
#include "api.h"

#include "keyHandlers.h"
#include "StarField.h"
#include "Menu.h"

namespace {
	int hit = 0;
	int lives = 3;
	int n1 = -1;
	int n2 = -1;
	int dead = 0;
	float y = 125;
	float fn = 0;
}

Xwing::Xwing() {
	hit = 0;
	lives = 3;
	n1 = -1;
	n2 = -1;
	dead = 0;
	y = 125;
	RegisterMessage("HeroHit", this);
	RegisterMessage("SetNumbers", this);
}

void Xwing::Update() {
	if (dead == 0) {
		if (n1 != -1) PrintChar(20, 115, n1 + 48, 0, 255, 0);
		if (n2 != -1) {
			PrintChar(27, 115, n2 + 48, 0, 255, 0);
			n1 = n2 = -1;
		}
		if (hit > 0) hit--;
		//Tint(255, 0, 0);
		fn += 0.05f;
		y = 125 + SDL_sinf(fn)*10.0f;
		if ((hit / 15) % 2 == 0) Draw(20, y, 0, 64, 37, 21, SDL_cosf(fn)*5.0f);
		//Tint(255, 255, 255);
	}
	else {
		dead--;
		if (lives == 0) {
			if (dead < 120) Print(200, 120, "GAME OVER", 255, 255, 255);
			if (dead == 0) {
				Reset();
				RegisterSystem(new StarField());
				RegisterSystem(new Menu());
				RegisterKeyboardHandler(&menuKeyHandler);
			}
		} else {
			if (dead == 0) SendMessage("ResetEnemies");
		}
	}
}

void HeroHit(int x, int y) {
	SendMessage("CreateNewLaser", x, y, 40, 125, 1);
	hit = 240;
	lives--;
	dead = lives == 0 ? 300 : 180;
	SendMessage("DecreaseLives");
	//if (lives == 0) 
		SendMessage("GenerateExplosion", 60, 135);
}

void SetNumbers(int p1, int p2) {
	if (p1 >= 0) n1 = p1;
	if (p2 >= 0) n2 = p2;
}

void Xwing::ProcessMessage(const char* msg) {
	if (message_eq("HeroHit")) {
	//if (msg == "HeroHit") {
		int* params = GetMessageParams();
		HeroHit(params[0], params[1]);
	} else if (message_eq("SetNumbers")) {
		int* params = GetMessageParams();
		SetNumbers(params[0], params[1]);
	}
}
