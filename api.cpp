#include "api.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include <string>
#include <vector>
#include <algorithm>

SDL_Window* sdlWindow = NULL;
SDL_Renderer* sdlRenderer = NULL;
SDL_Texture* sdlTexture = NULL;
SDL_Event sdlEvent;
SDL_Scancode keyJustPressed = SDL_SCANCODE_UNKNOWN;
const Uint8* keyboard = nullptr;
bool blink = false;
KeyboardHandler keyboardHandler = NULL;
std::vector<System*> systems;
std::vector<std::string> msgTexts;
std::vector<System*> msgSystems;
int msgParams[5];
int msgReturn;
bool reseted = false;
bool anyKey = false;

void Init() {
	SDL_Init(SDL_INIT_EVERYTHING);
	sdlWindow = SDL_CreateWindow("Math Wars v0.1", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 960, 540, SDL_WINDOW_FULLSCREEN_DESKTOP); // SDL_WINDOW_SHOWN
	sdlRenderer = SDL_CreateRenderer(sdlWindow, -1, SDL_RENDERER_PRESENTVSYNC);
	SDL_SetRenderDrawBlendMode(sdlRenderer, SDL_BLENDMODE_BLEND);
	SDL_RenderSetLogicalSize(sdlRenderer, 480, 270);
}

void Quit() {
	SDL_DestroyWindow(sdlWindow);
	SDL_Quit();
}

void LoadImage(const char* filename) {
	int w, h, c;
	FILE* f = fopen(filename, "rb");
	//if (!f) { error = 2; return; }
	unsigned char* buffer = stbi_load_from_file(f, &w, &h, &c, 4);
	if (sdlTexture) SDL_DestroyTexture(sdlTexture);
	sdlTexture = SDL_CreateTexture(sdlRenderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STATIC, w, h);
	SDL_UpdateTexture(sdlTexture, NULL, buffer, w * sizeof(Uint32));
	SDL_SetTextureBlendMode(sdlTexture, SDL_BLENDMODE_BLEND);
	fclose(f);
}

void Flip() {
	SDL_RenderPresent(sdlRenderer);
}

void Clear() {
	if (blink) {
		SDL_SetRenderDrawColor(sdlRenderer, 255, 255, 255, 255);
	}
	else {
		SDL_SetRenderDrawColor(sdlRenderer, 0, 0, 0, 255);
	}
	blink = false;
	SDL_RenderClear(sdlRenderer);
}

void Blink() {
	blink = true;
}

void Draw(int x, int y, int sx, int sy, int w, int h, int angle) {
	SDL_Rect src, dst;
	src.x = sx; src.y = sy;
	dst.x = x; dst.y = y;
	src.w = dst.w = w;
	src.h = dst.h = h;
	SDL_RenderCopyEx(sdlRenderer, sdlTexture, &src, &dst, angle, NULL, SDL_FLIP_NONE);
}

void DrawScale(int x, int y, int sx, int sy, int w, int h, float scale) {
	SDL_Rect src, dst;
	src.x = sx; src.y = sy;
	dst.x = x; dst.y = y;
	src.w = w;  dst.w = w*scale;
	src.h = h;  dst.h = h*scale;
	SDL_RenderCopy(sdlRenderer, sdlTexture, &src, &dst);
}

void DrawEx(int x, int y, int sx, int sy, int w, int h, float scale, int angle) {
	SDL_Rect src, dst;
	float xx = x - (w*scale - w) / 2;
	float yy = y - (h*scale - h) / 2;
	src.x = sx; src.y = sy;
	dst.x = xx; dst.y = yy;
	src.w = w;  dst.w = w*scale;
	src.h = h;  dst.h = h*scale;
	SDL_RenderCopyEx(sdlRenderer, sdlTexture, &src, &dst, angle, NULL, SDL_FLIP_NONE);
}

void Print(int x, int y, const char* text, int r, int g, int b) {
	int str_size = strlen(text);
	SDL_SetTextureColorMod(sdlTexture, r, g, b);
	for (int i = 0; i < str_size; i++) {
		char chr = text[i];
		Draw(x + (i * 8), y, (chr & 0x0f) * 8, (chr >> 4) * 8, 8, 8);
	}
	SDL_SetTextureColorMod(sdlTexture, 255, 255, 255);
}

void PrintChar(int x, int y, const char text, int r, int g, int b) {
	SDL_SetTextureColorMod(sdlTexture, r, g, b);
	Draw(x, y, (text & 0x0f) * 8, (text >> 4) * 8, 8, 8);
	SDL_SetTextureColorMod(sdlTexture, 255, 255, 255);
}

void SetColor(int r, int g, int b) {
	SDL_SetRenderDrawColor(sdlRenderer, r, g, b, 255);
}

void DrawPoint(int x, int y) {
	SDL_RenderDrawPoint(sdlRenderer, x, y);
}

void DrawLine(int x1, int y1, int x2, int y2) {
	SDL_RenderDrawLine(sdlRenderer, x1, y1, x2, y2);
}

void Tint(int r, int g, int b) {
	SDL_SetTextureColorMod(sdlTexture, r, g, b);
}

void SetBlend(BlendMode mode) {
	SDL_BlendMode m = mode == BlendNone ? SDL_BLENDMODE_NONE : mode == BlendBlend ? SDL_BLENDMODE_BLEND : mode == BlendAdd ? SDL_BLENDMODE_ADD : SDL_BLENDMODE_MOD;
	SDL_SetTextureBlendMode(sdlTexture, m);
}

void SetAlpha(int alpha) {
	SDL_SetTextureAlphaMod(sdlTexture, alpha);
}

void RegisterKeyboardHandler(KeyboardHandler handler) {
	keyboardHandler = handler;
}

void RegisterSystem(System* system) {
	systems.push_back(system);
}

void RemoveSystem(System* system) {
	systems.erase(std::remove(systems.begin(), systems.end(), system), systems.end());
}

void Reset() {
	systems.clear();
	msgSystems.clear();
	msgTexts.clear();
	reseted = true;
}

bool Update() {
	while (SDL_PollEvent(&sdlEvent)) {
		if (sdlEvent.type == SDL_QUIT) { return true; break; }
		else if (sdlEvent.type == SDL_KEYDOWN) {
			anyKey = true;
			keyJustPressed = sdlEvent.key.keysym.scancode;
			if (sdlEvent.key.keysym.scancode == SDL_SCANCODE_ESCAPE) { return true; }
			if (keyboardHandler) keyboardHandler(keyJustPressed);
			anyKey = false;
		}
	}
	Clear();
	for (auto system : systems) {
		system->Update();
		if (reseted) {
			reseted = false; break;
		}
	}
	Flip();
	return false;
}

void RegisterMessage(const char* msg, System* handler) {
	msgTexts.push_back(msg);
	msgSystems.push_back(handler);
}

void SendMessage(const char* msg, int p1, int p2, int p3, int p4, int p5) {
	msgParams[0] = p1; msgParams[1] = p2; msgParams[2] = p3; msgParams[3] = p4; msgParams[4] = p5;
	auto it = std::find(msgTexts.begin(), msgTexts.end(), msg);
	if (it != msgTexts.end()) {
		auto index = std::distance(msgTexts.begin(), it);
		msgSystems[index]->ProcessMessage(msg);
	}
}

int* GetMessageParams() {
	return msgParams;
}

int GetMessageReturn() {
	return msgReturn;
}

void SetMessageReturn(int value) {
	msgReturn = value;
}
