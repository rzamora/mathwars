#pragma once

class System {
public:
	virtual ~System() {};
	virtual void Update() = 0;
	virtual void ProcessMessage(const char* msg) {};
};

