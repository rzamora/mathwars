#include "Explosions.h"
#include "api.h"

#define MAX_EXPLOSIONS 5
#define PIXELS_PER_EXPLOSION 24
#define PARTICLES_PER_EXPLOSION 4
struct Particle {
	Point pos;
	Point dir;
	float scale;
	float angle;
};
struct Explosion {
	Point pos;
	Point dir;
	float scale;
	float angle;
	int ttl;
};
Explosion explosions[MAX_EXPLOSIONS][PIXELS_PER_EXPLOSION];
Particle particles[MAX_EXPLOSIONS][PARTICLES_PER_EXPLOSION];

Explosions::Explosions() {
	for (int i = 0; i < MAX_EXPLOSIONS; i++) {
		explosions[i][0].ttl = 0;
	}
	RegisterMessage("GenerateExplosion", this);
}


void Explosions::Update() {
	for (int i = 0; i < MAX_EXPLOSIONS; i++) {
		if (explosions[i][0].ttl > 0) {
			for (int j = 0; j < PIXELS_PER_EXPLOSION; j++) {
				explosions[i][j].pos.x += explosions[i][j].dir.x;
				explosions[i][j].pos.y += explosions[i][j].dir.y;
				//SetColor(128+rand()%128, 128+rand()%128, 128+rand()%128);
				//DrawPoint(explosions[i][j].pos.x, explosions[i][j].pos.y);
				Tint(128 + rand() % 128, 128 + rand() % 128, 128 + rand() % 128);
				DrawEx(explosions[i][j].pos.x, explosions[i][j].pos.y, 3, 85, 1, 1, explosions[i][j].scale, explosions[i][j].angle);
				if (explosions[i][j].scale > 1) explosions[i][j].scale -= 0.01f;
				explosions[i][j].angle += explosions[i][j].dir.x + explosions[i][j].dir.y;
			}
			explosions[i][0].ttl--;

			SetBlend(BlendAdd);
			SetAlpha(128);
			for (int j = 0; j < 4; j++) DrawEx(particles[i][j].pos.x, particles[i][j].pos.y, 0, 85, 10, 10, particles[i][j].scale, particles[i][j].angle);
			Tint(255, 128, 0);
			SetAlpha(64);
			for (int j = 0; j < 4; j++) DrawEx(particles[i][j].pos.x, particles[i][j].pos.y, 0, 85, 10, 10, particles[i][j].scale * 2, particles[i][j].angle);
			SetBlend(BlendBlend);
			Tint(255, 255, 255);
			SetAlpha(255);
			for (int j = 0; j < 4; j++) {
				if (particles[i][j].scale > 0.0f) {
					particles[i][j].pos.x += particles[i][j].dir.x;
					particles[i][j].pos.y += particles[i][j].dir.y;
					particles[i][j].scale -= 0.1f;
				}
			}
		}
	}
}

void GenerateExplosion(int x, int y) {
	for (int i = 0; i < MAX_EXPLOSIONS; i++) {
		if (explosions[i][0].ttl == 0) {
			explosions[i][0].ttl = 60;
			for (int j = 0; j < PIXELS_PER_EXPLOSION; j++) {
				explosions[i][j].pos.x = x;
				explosions[i][j].pos.y = y;
				explosions[i][j].dir.x = ((rand() % 200) - 100)*0.1f;
				explosions[i][j].dir.y = ((rand() % 200) - 100)*0.1f;
				explosions[i][j].scale = 1 + (rand() % 100)*0.04f;
				explosions[i][j].angle = rand() % 360;
			}
			for (int j = 0; j < PARTICLES_PER_EXPLOSION; j++) {
				particles[i][j].pos.x = x;
				particles[i][j].pos.y = y;
				particles[i][j].dir.x = ((rand() % 200) - 100)*0.02f;
				particles[i][j].dir.y = ((rand() % 200) - 100)*0.02f;
				particles[i][j].scale = 4;
				particles[i][j].angle = rand() % 360;
			}
		}
	}
}

void Explosions::ProcessMessage(const char* msg) {
	if (message_eq("GenerateExplosion")) {
		int* params = GetMessageParams();
		GenerateExplosion(params[0], params[1]);
	}
}
