#pragma once

#include <SDL2/SDL.h>
#include "System.h"
#include <stdlib.h>

#define message_eq(m) strncmp(msg, m, 20)
 
enum BlendMode { BlendNone, BlendBlend, BlendAdd, BlendMod };

struct Point {
	float x, y;
};

typedef void(*KeyboardHandler)(SDL_Scancode);

void Init();
void Quit();

void LoadImage(const char* filename);

void Flip();
void Clear();
void Blink();

void Draw(int x, int y, int sx, int sy, int w, int h, int angle = 0);
void DrawScale(int x, int y, int sx, int sy, int w, int h, float scale = 1.0f);
void DrawEx(int x, int y, int sx, int sy, int w, int h, float scale, int angle);
void Print(int x, int y, const char* text, int r, int g, int b);

void PrintChar(int x, int y, const char text, int r, int g, int b);

void SetColor(int r, int g, int b);
void DrawPoint(int x, int y);
void DrawLine(int x1, int y1, int x2, int y2);

void Tint(int r, int g, int b);
void SetBlend(BlendMode mode);
void SetAlpha(int alpha);

void RegisterKeyboardHandler(KeyboardHandler handler);

void RegisterSystem(System* system);
void RemoveSystem(System* system);
void Reset();
bool Update();

void RegisterMessage(const char* msg, System* handler);
void SendMessage(const char* msg, int p1 = 0, int p2 = 0, int p3 = 0, int p4 = 0, int p5 = 0);

int* GetMessageParams();
int GetMessageReturn();
void SetMessageReturn(int value);
