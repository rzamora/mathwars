#include "api.h"
#include <unistd.h>

#include "keyHandlers.h"
#include "StarField.h"
#include "Menu.h"

int main(int argc, char* argv[]) {
	srand(getpid());
	Init();
	LoadImage("gfx.png");

	RegisterSystem(new StarField());
	RegisterSystem(new Menu());
	RegisterKeyboardHandler(&menuKeyHandler);

	while (!Update()) {}
	Quit();
	return 0;
}
