MathWars v 0.algo
=================

Que seto?
---------
Un joc que vaig fer a principis de 2016 per a que el meu fill, fan d'Star Wars, practicara les tables de multiplicar.

Que fa falta?
-------------
Visual Studio 2017. Per ara nomes est� per a Windows. Ja el preparar� mes avant per a macOS i Linux.
Per a jugar tamb� et far� falta SDL2.dll, que pots ficar, segons les teues prefer�ncies:

-	En el directori del projecte, si l'executes desde el Visual Studio

-	En el directori del executable, si l'executes por si mism

-	En el windows/System32 i a tomar por culo


Com se chua?
------------
Tu eres el Ala-X eixe que veus, encara que no pugues moure'l. Te van apareguent Tie Fighters i Tie Bombers (ninguna
diferencia, nomes el dibuixet) amb una multiplicaci� al cap. Has de escriure el resultat usant el teclat num�ric per
a rebentar-los. Per exemple, si apareix un meim amb l'operaci� "3x4" de sombrero, has de pulsar la tecla "1" i despr�s la tecla "2"
del teclat numeric, i autom�gicament el Ala-X rebentar� al colega aquell. No es pot corregir si pulses la tecla
incorrecta (ni tampoc te penalitzaci�, a part de que se t'acosten els malos implacablement), aix� que si, en l'exemple
d'abans la primera tecla has polsat per error "4", polsa altra tecla per a acabar el numero actual, i tornes a comen�ar.
El meu fill de 8 anys ho va entendre a la primera aix� que ja hi ha prou de explicar.

Si els malotes s'acosten lo suficient, te petar�n el mame i perdr�s una vida.

Cada oleada es una tabla de multiplicar, comen�ant per la del 2. Cada oleada presentar� dos vegades cada operaci� de la seua tabla
de forma aleatoria. Per no fer el meim, no apareixen multiplicaci�ns per 0, per 1 ni per 10.

El joc no est� limitat, aix� que despr�s de la tabla del 9... mostrar� la del 10, 11, 12.... amb simbols raros i, clar, al final
amb l'imposibilitat de guanyar, per no poder ficar resultats de mes de 2 xifres. Es lo que t� no estar acabat.

Alg�n dia me molaria fer mes nivells o modes de joc. Per exemple, varies multiplicacions r�pidament per a destruir un AT-AT en Hoth,
divisions per a esquivar els turbolasers del corredor de la Estrella de la Muerte, que se jo...


JailDoctor 2020

![picture](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)
Aquesta obra est� baix una [llic�ncia de Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](http://creativecommons.org/licenses/by-sa/4.0/)
