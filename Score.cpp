#include "Score.h"
#include "api.h"

namespace {
	int lives = 3;
	int score = 0;
}

Score::Score() {
	lives = 3;
	score = 0;
	RegisterMessage("IncreaseScore", this);
	RegisterMessage("DecreaseLives", this);
}

void Score::Update() {
	char strScore[10];
	sprintf(strScore, "%d", score);
	//itoa(score, strScore, 10);
	Print(220, 4, strScore, 255, 255, 0);
	if (lives > 0) Draw(5, 5, 10, 85, 18, 18);
	if (lives > 1) Draw(25, 5, 10, 85, 18, 18);
	if (lives > 2) Draw(45, 5, 10, 85, 18, 18);
}

void Score::ProcessMessage(const char* msg) {
	if (message_eq("IncreaseScore")) {
		int* params = GetMessageParams();
		score += params[0];
	} else if (message_eq("DecreaseLives")) {
		lives--;
	}
}
