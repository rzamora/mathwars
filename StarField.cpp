#include "StarField.h"
#include "api.h"

Point starField[3][24];
float speed = 1;

StarField::StarField() {
	speed = 1;
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 24; j++) {
			starField[i][j].x = rand() % 480;
			starField[i][j].y = rand() % 270;
		}
	}
	RegisterMessage("SetStarFieldVelocity", this);
}

void StarField::Update() {
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 24; j++) {
			starField[i][j].x -= (speed + i*speed);
			if (starField[i][j].x < 0) {
				starField[i][j].x = 480;
				starField[i][j].y = rand() % 270;
			}
		}
	}
	SetColor(32, 32, 32);
	for (int j = 0; j < 24; j++) DrawPoint(starField[0][j].x, starField[0][j].y);
	SetColor(96, 96, 96);
	for (int j = 0; j < 24; j++) DrawPoint(starField[1][j].x, starField[1][j].y);
	SetColor(192, 192, 192);
	for (int j = 0; j < 24; j++) DrawPoint(starField[2][j].x, starField[2][j].y);
}

void StarField::ProcessMessage(const char* msg) {
	if (message_eq("SetStarFieldVelocity")) {
		int* params = GetMessageParams();
		speed = params[0] * 0.01f;
	}
}
