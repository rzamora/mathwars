#include "Enemies.h"
#include "api.h"

#define MAX_ENEMIES 5
struct Enemy {
	float x, y, by, fn;
	int m1, m2;
	int state;
	int type;
};
Enemy enemies[MAX_ENEMIES];
int enemyTimer = 0;
int kills = 0;
int wave = 1;
int betweenWaves = 300;
int generations = 0;
int table[8] = { 0, 1, 2, 3, 4, 5, 6, 7 };
bool visited[8] = { false, false, false, false, false, false, false, false };
int tableSize = 8;

void ResetEnemies() {
	enemyTimer = 0;
	kills = 0;
	generations = 0;
	tableSize = 8;
	for (int i = 0; i < 8; i++) { table[i] = i; visited[i] = false; }
	for (int i = 0; i < MAX_ENEMIES; i++) {
		enemies[i].x = enemies[i].y = enemies[i].by = -1;
		enemies[i].fn = 0;
		enemies[i].state = 0;
		enemies[i].type = rand() % 2;
	}
}

Enemies::Enemies() {
	ResetEnemies();
	RegisterMessage("CheckEnemyHit", this);
	RegisterMessage("ResetEnemies", this);
}

void GenerateNewEnemy() {
	if (generations >= 16) return;
	enemyTimer = 60 + SDL_max(60 - (kills), 0);
	for (int i = 0; i < MAX_ENEMIES; i++) {
		if (enemies[i].y == -1) {
			generations++;
			enemies[i].by = enemies[i].y = 20 + rand() % 230;
			enemies[i].fn = 0;
			enemies[i].x = 480;
			enemies[i].m1 = 1 + wave; // (rand() % (3 + SDL_min(int(SDL_floor(kills / 5.0f)), 4)));
			int num = rand() % tableSize;
			/*char peiv[10]; itoa(table[num], peiv, 10);
			SDL_Log(peiv);*/
			enemies[i].m2 = 2 + table[num];
			if (visited[table[num]]) { tableSize--;  if (num < tableSize) table[num] = table[tableSize]; } else { visited[table[num]] = true; }
			enemies[i].state = 0;
			enemies[i].type = rand() % 2;
			return;
		}
	}
}

void Enemies::Update() {
	if (betweenWaves > 0) {
		betweenWaves--;
		if (betweenWaves < 240) {
			char waveText[12] = "TABLA DEL 0"; waveText[10] = 1+wave + 48;
			Print(200, 120, waveText, 255, 255, 255);
		}
	} else {
		enemyTimer--;
		if (enemyTimer <= 0) GenerateNewEnemy();
		for (int i = 0; i < MAX_ENEMIES; i++) {
			if (enemies[i].y > -1) {
				enemies[i].fn += (0.01f + kills*0.0025f);
				enemies[i].y = enemies[i].by + SDL_sinf(enemies[i].fn)*10.0f;
				enemies[i].x -= (0.5f + kills*0.02f);
				if (enemies[i].state == 0 && enemies[i].x <= 100) {
					SendMessage("CreateNewLaser", enemies[i].x, enemies[i].y, 40, 125, 1);
					Blink();
					SendMessage("HeroHit");
					enemyTimer = 400;
					for (int j = 0; j < MAX_ENEMIES; j++) {
						if (enemies[j].y < 135) { enemies[j].state = 10; }
						else { enemies[j].state = -10; }
						enemies[j].m1 = enemies[j].m2 = 0;
					}
					//betweenWaves = 420; ResetEnemies();
				}
				if (enemies[i].state != 0) enemies[i].x -= 1.0f;
				if (enemies[i].state > 0) enemies[i].y -= 1.0f;
				if (enemies[i].state < 0) enemies[i].y += 1.0f;

				if (enemies[i].type == 0) {
					Draw(enemies[i].x, enemies[i].y, 37, 64, 17, 13, enemies[i].state - SDL_cosf(enemies[i].fn)*10.0f);
				}
				else {
					Draw(enemies[i].x, enemies[i].y, 54, 64, 21, 13, enemies[i].state - SDL_cosf(enemies[i].fn)*10.0f);
				}
				if (enemies[i].state == 0) {
					PrintChar(enemies[i].x - 2, enemies[i].y - 10, enemies[i].m1 + 48, 0, 255, 0);
					PrintChar(enemies[i].x + 5, enemies[i].y - 10, 'x', 128, 128, 128);
					PrintChar(enemies[i].x + 12, enemies[i].y - 10, enemies[i].m2 + 48, 0, 255, 0);
				}
				if (enemies[i].x < -20) { enemies[i].x = enemies[i].y = -1; }
			}
		}
	}
}

int CheckEnemyHit(int n1, int n2) {
	for (int i = 0; i < MAX_ENEMIES; i++) {
		if (enemies[i].y > -1) {
			if (enemies[i].m1 * enemies[i].m2 == n1 * 10 + n2) {
				SendMessage("CreateNewLaser", 40, 125, enemies[i].x, enemies[i].y, 0);
				Blink();
				kills++;
				SendMessage("GenerateExplosion", enemies[i].x, enemies[i].y); //GenerateExplosion(enemies[i].x, enemies[i].y);
				enemies[i].x = enemies[i].y = -1;
				SendMessage("IncreaseScore", 100); //score += 100;
				if (kills == 16) {
					betweenWaves = 360; wave++; ResetEnemies();
				}
				return 1;
			}
		}
	}
	return 0;
}

void Enemies::ProcessMessage(const char* msg) {
	if (strcmp(msg, "CheckEnemyHit") == 0) {
		int* params = GetMessageParams();
		SetMessageReturn(CheckEnemyHit(params[0], params[1]));
	} else if (strcmp(msg, "ResetEnemies") == 0) {
		betweenWaves = 420; ResetEnemies();
	}
}
