
#include "api.h"

#include "keyHandlers.h"

#include "StarField.h"
#include "Explosions.h"
#include "Lasers.h"
#include "Enemies.h"
#include "Xwing.h"
#include "Score.h"

int digit = -1;

void keyHandler(SDL_Scancode key) {
	int num = 0;
	if (key >= 89 && key <= 98) {
		num = key == 98 ? 0 : key - 88;
	} else if (key >= 30 && key <= 39) {
		num = key == 39 ? 0 : key - 29;
	} else {
		return;
	}

	if (digit == -1) {
		digit = num;
		SendMessage("CheckEnemyHit", 0, num);
		if (GetMessageReturn() != 0) {
			SendMessage("SetNumbers", -1, num);
			digit = -1;
		} else {
			SendMessage("SetNumbers", num, -1);
		}
	}
	else {
		SendMessage("SetNumbers", -1, num);
		SendMessage("CheckEnemyHit", digit, num);
		digit = -1;
	}
}

void menuKeyHandler(SDL_Scancode key) {
	Reset();
	RegisterSystem(new StarField());
	RegisterSystem(new Xwing());
	RegisterSystem(new Enemies());
	RegisterSystem(new Lasers());
	RegisterSystem(new Explosions());
	RegisterSystem(new Score());
	RegisterKeyboardHandler(&keyHandler);
}
